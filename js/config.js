window._config = {
    cognito: {
        userPoolId: 'eu-west-2_TRj97XV3G', // e.g. us-east-2_uXboG5pAb
        userPoolClientId: '1mbl97a6f6htru8qd1t2rconjl', // e.g. 25ddkmj4v6hfsfvruhpfi7n4hv
        region: 'eu-west-2' // e.g. us-east-2
    },
    api: {
        invokeUrl: 'https://liv48m2asi.execute-api.eu-west-2.amazonaws.com/prod' // e.g. https://rc7nyt4tql.execute-api.us-west-2.amazonaws.com/prod',
    }
};
